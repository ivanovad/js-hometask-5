# js-hometask-5

## Ответьте на вопросы

1. Какие данные хранятся в следующих свойствах узла: `parentElement`, `children`, `previousElementSibling` и `nextElementSibling`?
> Ответ: 
> parentElement - ссылка на родительский элемент (узел, в который вложен данный элемент) 
> children - HTMLCollection, в которой содержатся все дочерние элементы данного узла c типом ELEMENT_NODE (ELEMENT_NODE - узел, который образован от какого-то тега: div, p и т.д.)
> previousElementSibling - ссылка на предыдущий узел с типом ELEMENT_NODE 
> nextElementSibling - ссылка на следующий узел с типом ELEMENT_NODE
2. Нарисуй какое получится дерево для следующего HTML-блока.

```html
<p>Hello,<!--MyComment-->World!</p>
```
> ответ в файле дз.png

## Выполните задания

1. Клонируй репозиторий;
2. Допиши функции в `tasks.js`;
3. Для проверки работы функций открой index.html в браузере;
4. Создай MR с решением.
