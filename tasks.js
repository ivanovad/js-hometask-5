"use strict";
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
 *   1. Удали со страницы элемент с id "deleteMe"
 **/

function removeBlock() {
  const deletedElem = document.getElementById("deleteMe");
  deletedElem.remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wrapper = document.getElementsByClassName("wrapper");

  for (let elem of wrapper) {
    const paragraphs = elem.querySelectorAll("p");

    let resultSum = 0;
    paragraphs.forEach((paragraph) => {
      resultSum += Number(paragraph.textContent);
      paragraph.remove();
    });

    const resultParagaph = document.createElement("p");
    resultParagaph.textContent = resultSum;

    elem.appendChild(resultParagaph);
  }
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const changeMeElem = document.getElementById("changeMe");

  changeMeElem.value = "1234";
  changeMeElem.type = "button";
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  const changeChild = document.getElementById("changeChild");

  const liOne = document.createElement("li");
  liOne.textContent = 1;
  changeChild.insertBefore(liOne, changeChild.children[1]);

  const liThree = document.createElement("li");
  liThree.textContent = 3;
  changeChild.appendChild(liThree);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  let itemClassElements = document.getElementsByClassName("item");

  for (const elem of itemClassElements) {
    if (elem.classList.contains("red")) {
      elem.classList.remove("red");
      elem.classList.add("blue");
    } else if (elem.classList.contains("blue")) {
      elem.classList.remove("blue");
      elem.classList.add("red");
    }
  }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
